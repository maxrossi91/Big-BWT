CMAKE_MINIMUM_REQUIRED(VERSION 3.15)

# About this project
# ------------------
project(Big-BWT)
SET(VERSION_MAJOR "1")
SET(VERSION_MINOR "0")
SET(VERSION_PATCH "0")
SET(VERSION "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")

# Set environment
# ------------------
set(CMAKE_C_COMPILER gcc)

SET(CMAKE_C_FLAGS "-O3 -Wall -std=c99 -g")
SET(CMAKE_CXX_FLAGS "-std=c++11 -O3 -Wall -Wextra -g")

set(CMAKE_INSTALL_PREFIX ${CMAKE_BINARY_DIR}/install)

# Set object libraries
# ------------------
add_library(malloc_count.o OBJECT malloc_count.c malloc_count.h) # Object library
target_compile_options(malloc_count.o PRIVATE -c)

add_library(utils.o OBJECT utils.c utils.h) # Object library
target_compile_options(utils.o PRIVATE -c)

add_library(xerrors.o OBJECT xerrors.c xerrors.h) # Object library
target_compile_options(xerrors.o PRIVATE -c)


# Set executable files
# ------------------

add_executable(bwtparse bwtparse.c)
target_link_libraries(bwtparse gsacak.o malloc_count.o utils.o -ldl)

add_executable(bwtparse64 bwtparse.c)
target_compile_definitions(bwtparse64 PRIVATE -DM64)
target_link_libraries(bwtparse64 gsacak.o malloc_count.o utils.o -ldl)

add_executable(simplebwt simplebwt.c)
target_link_libraries(simplebwt gsacak.o)

add_executable(simplebwt64 simplebwt.c)
target_compile_definitions(simplebwt64 PRIVATE -DM64)
target_link_libraries(simplebwt64 gsacak.o)

# cnewscan executable to scan gzipped files (currently not active)
# add_executable(cnewscan.x newscan.cpp)
# target_compile_definitions(cnewscan.x PRIVATE -DGZSTREAM -DNOTHREADS)
# target_link_libraries(cnewscan.x malloc_count.o utils.o -lgzstream -lz -ldl)

add_executable(newscanNT.x newscan.cpp)
target_compile_definitions(newscanNT.x PRIVATE -DNOTHREADS)
target_link_libraries(newscanNT.x malloc_count.o utils.o -ldl)

add_executable(newscan.x newscan.cpp newscan.hpp)
target_link_libraries(newscan.x malloc_count.o utils.o xerrors.o -ldl -pthread)

add_executable(pscan.x pscan.cpp pscan.hpp)
target_link_libraries(pscan.x malloc_count.o utils.o xerrors.o -ldl -pthread)

# prefix free BWT construction
add_executable(pfbwt.x pfbwt.cpp pfthreads.hpp)
target_link_libraries(pfbwt.x gsacak.o malloc_count.o utils.o xerrors.o -ldl -pthread)

add_executable(pfbwt64.x pfbwt.cpp pfthreads.hpp)
target_compile_definitions(pfbwt64.x PRIVATE -DM64)
target_link_libraries(pfbwt64.x gsacak.o malloc_count.o utils.o xerrors.o -ldl -pthread)

# TO BE REMOVED? (now pfbwt*.x works with malloc_count)
# prefix free BWT construction without threads: useful since supports malloc_count
add_executable(pfbwtNT.x pfbwt.cpp)
target_compile_definitions(pfbwtNT.x PRIVATE -DNOTHREADS)
target_link_libraries(pfbwtNT.x gsacak.o malloc_count.o utils.o -ldl)

# as above for large files
add_executable(pfbwtNT64.x pfbwt.cpp)
target_compile_definitions(pfbwtNT64.x PRIVATE -DNOTHREADS -DM64)
target_link_libraries(pfbwtNT64.x gsacak.o malloc_count.o utils.o -ldl)

add_executable(unparse unparse.c)
target_link_libraries(unparse malloc_count.o utils.o -ldl)

add_executable(unparsz unparsz.c)
target_link_libraries(unparsz malloc_count.o utils.o -ldl)

add_executable(remap remap.c)
target_compile_options(remap PRIVATE -O -g -Wall)
target_link_libraries(remap -lm)

add_custom_target(bigbwt ALL COMMAND
${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/bigbwt ${CMAKE_CURRENT_BINARY_DIR})

set(SOURCE_FILES  ${CMAKE_SOURCE_DIR}/bigbwt
                  ${CMAKE_SOURCE_DIR}/newscan.[ch]pp
                  ${CMAKE_SOURCE_DIR}/pscan.[ch]pp
                  ${CMAKE_SOURCE_DIR}/pfbwt.cpp
                  ${CMAKE_SOURCE_DIR}/pfthreads.hpp
                  ${CMAKE_SOURCE_DIR}/simplebwt.c
                  ${CMAKE_SOURCE_DIR}/bwtparse.c
                  ${CMAKE_SOURCE_DIR}/unparse.c
                  ${CMAKE_SOURCE_DIR}/remap.c
                  ${CMAKE_SOURCE_DIR}/makefile
                  ${CMAKE_SOURCE_DIR}/utils.[ch]
                  ${CMAKE_SOURCE_DIR}/xerrors.[ch]
                  ${CMAKE_SOURCE_DIR}/f2s.py
                  ${CMAKE_SOURCE_DIR}/gsa/gsacak.[ch]
                  ${CMAKE_SOURCE_DIR}/gsa/LICENSE
                  ${CMAKE_SOURCE_DIR}/gsa/README.md
                  ${CMAKE_SOURCE_DIR}/malloc_count.[ch])

add_custom_target(tarfile COMMAND
${CMAKE_COMMAND} -E tar zcf bigbwt.tgz ${SOURCE_FILES})

add_subdirectory(gsa)
